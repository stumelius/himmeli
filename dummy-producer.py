#!/usr/bin/env python3.6
import sys
import argparse
import logging
import random
import time
import json
from kafka import KafkaProducer
logging.basicConfig(
    filemode='w', 
    format='[%(asctime)s.%(msecs)03d] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s', 
    datefmt='%Y-%m-%d %H:%M:%S',
    level=logging.INFO,
    stream=sys.stdout
)
logger = logging.getLogger(__file__)
parser = argparse.ArgumentParser(
    description='Produce dummy temperature data to a Kafka topic.'
)
parser.add_argument(
    '--kafka-url', 
    help='URL of the Kafka server.',
    type=str,
    required=True
)
parser.add_argument(
    '--topic', 
    help='Topic to producer the data to.',
    type=str,
    required=True
)
parser.add_argument(
    '--freq', 
    help='Data generation frequency.',
    type=int,
    required=True
)
parser.add_argument(
    '--device-id', 
    help='Producer device identifier.',
    type=str,
    required=True
)
parser.add_argument(
    '--header', 
    help='Message header.',
    type=str,
    default='temperature_C'
)


if __name__ == '__main__':
    args = parser.parse_args()
    producer = KafkaProducer(bootstrap_servers=args.kafka_url, value_serializer=lambda v: json.dumps(v).encode('utf-8'))
    while 1:
        try:
            message_dict = {'device_id': args.device_id, args.header: random.random()}
            future = producer.send(args.topic, message_dict)
            # Block until a single message is sent (or timeout)
            result = future.get(timeout=60)
            logger.info(f'Sent {message_dict} (timestamp: {result.timestamp}) to {args.kafka_url} (topic: {args.topic})')
            time.sleep(1/args.freq)
        except KeyboardInterrupt:
            logger.info('Keyboard interrupt detected, exiting ...')
            break