[![Waffle.io - Columns and their card count](https://badge.waffle.io/smomni/himmeli.svg?columns=all)](https://waffle.io/smomni/himmeli)

# himmeli

> **himmeli**
> 1. An ornament made of interconnected straws or reeds


## Installation

To compile and install [Python 3.6.8](https://www.python.org/downloads/release/python-368/) and `pip` on Debian:

    sudo apt-get install build-essential checkinstall
    sudo apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
    cd ~
    sudo wget https://www.python.org/ftp/python/3.6.8/Python-3.6.8.tgz
    sudo tar xzf Python-3.6.8.tgz
    cd Python-3.6.8
    sudo ./configure
    sudo make altinstall
    sudo apt-get install python3-pip
    sudo python3.6 -m pip install --upgrade pip

Install `himmeli` to a Python virtual environment:

    git clone https://github.com/smomni/himmeli.git
    cd himmeli
    sudo python3.6 -m pip install virtualenv
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt
    pip install .

## Running the [infrastructure](docs/infrastructure.md)

Run the entire infrastructure on a single host with `docker-compose`:

```bash
docker-compose up
```

NOTE: If running on a Windows host, set environment variable `COMPOSE_CONVERT_WINDOWS_PATHS=1` (PowerShell: `$Env:COMPOSE_CONVERT_WINDOWS_PATHS=1`).

IMPORTANT: The infrastructure is run to advertise on port 9092 only on localhost. Therefore, SSH port-forwarding is required to connect to the Kafka server. 
To start port-forwarding: `ssh -L 9092:localhost:9092 user@kafka-server.com`


## Running a producer

Start a dummy producer that generates 10 samples per second:

```bash
dummy-producer.py --freq 10 --kafka-url localhost:9092 --topic dummy --device-id nanopi-01 --header voltage_V
```